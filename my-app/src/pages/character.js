import {UserStatsProvider, GameState} from '../context/index.js'
import React, { useContext, useEffect, } from 'react'

const User = React.lazy(()=> import('../components/users/index'))


const Character = () => {
const { isName, setName } = useContext(GameState)

useEffect(()=>{
  const getName = localStorage.getItem('name')
  if(getName.length > 0 ) { setName(true)}
},[isName])

  return (
     <div className="game_wrapper">   
     <UserStatsProvider>
     <User alive/>
     </UserStatsProvider>
     </div>
  );
}

export default Character;
