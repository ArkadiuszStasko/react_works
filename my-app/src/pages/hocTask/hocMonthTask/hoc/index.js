import CustomDate from './customDate'
import React, { Component } from 'react';


const withCustomDate = Component => props => {

    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    
    let newdate = month + "/" + day + "/" + year;
    

    return(
        <div>
            {
                props.date === newdate
                ? <CustomDate {...props} />
                : <Component {...props} />
            }
        </div>
    )
}

export default withCustomDate


