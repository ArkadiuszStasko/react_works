import {useState} from 'react'
import List from './todoListMap'
import './styles.css'

const TodoList = () => {
    const [data, setData] = useState([])

    const [inputValue, setInputValue] = useState('')

    const handleSumbit = (e) => {
        e.preventDefault()
        let today = new Date();

        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); 
        var yyyy = today.getFullYear();
        
        today = mm + '/' + dd + '/' + yyyy;
        setData([...data, 
        {
            title: inputValue, 
            date: today, 
            id: Math.random() * (1000 - 1 + 1) + 1
        }
    ])
    }

    return(
        <div>
           <h1>Todo List</h1>
           <form onSubmit={e => handleSumbit(e)}>
               <input value={inputValue} onChange={(e)=>setInputValue(e.target.value)} type='text'></input>
               <button type='submit'>Add</button>   
           </form>
        {data.map(item=>{
           return( <List data={{item, data, setData}} key={item.id} /> )
        })}
        </div>
    )
}

export default TodoList