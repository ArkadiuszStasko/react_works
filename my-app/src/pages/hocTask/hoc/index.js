import components from '../components'
import withCustomText from './withCustomText/index'
import Test from '../test'

components.Test = withCustomText(props => <Test {...props}/>)