import React, {useState} from 'react'
import { useGet } from 'restful-react'
import Carousel from 'react-elastic-carousel';
import './styles.css'

const Test = () => {

const [selectVaule, setSelect] = useState('akita')

const{ data: randomDogImage, loading, refetch } = useGet({
    path: `https://dog.ceo/api/breed/${selectVaule}/images/random`
})

const handleChange = (e) => {
    setSelect(e.target.value);
    console.log(selectVaule)
  }


    return(
        <div>
            <h1>Dog breeds</h1>
         {
             loading ? <h1>Loading...</h1> : (
             <div>

                 <div className='Image-wrapper'> 
                 <Carousel>
                 <div>
                 <img alt="doggy" src={randomDogImage.message} width="300px"/>
                 </div>
                 <div>
                 <img alt="doggy" src={randomDogImage.message} width="300px"/>
                 </div>
                 <div>
                 <img alt="doggy" src={randomDogImage.message} width="300px"/>
                 </div>
                 </Carousel>
                 </div>

<form>
<select value={selectVaule} onChange={e => handleChange(e)}>
  <option value="akita">Akita</option>
  <option value="beagle">Beagle</option>
  <option value="bulldog">Buldog</option>
  <option value="germanshepherd">Owczarek Niemiecki</option>
</select>
</form>

             </div>
             )
         }
        </div>
    )
}

export default Test