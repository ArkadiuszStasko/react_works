import React, { useRef, Suspense } from 'react'
import Header from '../components/header/index'


const World = () => {
const refPo = useRef(null)
const refL = useRef(null)
const refD = useRef(null)
const refPu = useRef(null)
const refG = useRef(null)


  return (
     <div>
      <Suspense fallback={
      <div>
       <h1>Ładowanie...</h1>
      </div>
    }>
       <Header {...{refPo, refL, refD, refPu, refG}}/>
       <div className="col-12 destination" ref={refPo}>Polana</div>
       <div className="col-12 destination" ref={refL}>Las</div>
       <div className="col-12 destination" ref={refD}>Dżungla</div>
       <div className="col-12 destination" ref={refPu}>Pustynia</div>
       <div className="col-12 destination" ref={refG}>Góry</div>
     </Suspense>
     </div>
  )
}

export default World;
