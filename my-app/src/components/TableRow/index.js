
import { useContext } from 'react';
import { CharStats } from '../../context/index'


const TableRow = (props) => {

const {name, str, hp, speed, dmg, lvl} = useContext(CharStats);
const myStats = { name, str, hp, speed, dmg, lvl };


    return(
    <div>
        <div>
        {
           props.render(myStats)
        }
        </div>
    </div>
    )
}

export default TableRow;