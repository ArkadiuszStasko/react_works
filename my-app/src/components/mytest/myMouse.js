import {useState} from 'react'


const MyMouse = (props) => {
    const [mousePos, setMousePos] = useState({'x':0, 'y':0})

    const handleMouseMove = el => {
        setMousePos({'x':el.clientX , 'y':el.clientY})
    }

    return(
        <div className='col-12 destination' onMouseMove={(el) => handleMouseMove(el)}>
            <h2>Rusz Myszką</h2>
            {
                props.render(mousePos)
            }
        </div>
    )
}

export default MyMouse