import './style.css';
import { useContext, useEffect } from 'react';
import { CharStats } from '../../context/index'
import Table from '../TableRow/table'
import Oponent from '../oponent';

const User = (props) => {
const {name, str, hp, speed, addStr, addName, dmg, lvl, addLvl, exp, addExp, maxexp} = useContext(CharStats);
const myStats = { name, str, hp, speed, dmg, lvl };

useEffect(()=>{
    addName(localStorage.getItem('name'))
    if(exp === maxexp){
        addExp(exp - 200)
        addLvl(lvl + 1)
    }
}, [exp])


    return(
        <div>
        <div className='users-container'>
    <div className="user-wrapper">
        <Table {...myStats} />
        <div className='exp-wrapper'>
            <div className='show_exp-wrapper'>
                <p>{exp}/{maxexp}</p>
            <div className='get_exp-wrapper' style={{width: (exp/2 + '%')}}></div>
            </div>
        </div>
        <div onClick={() =>addStr(str+1)}>add +1 str </div>
    </div>
    <div className='oponent-wrapper'>
    {props.alive &&
    <Oponent />
    }
    </div>
      </div>
       <div className='exp-btn' onClick={() =>addExp(exp+10)}>
         Add exp
       </div>
  </div>
    )
}

export default User;