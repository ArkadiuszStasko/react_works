import {UserStatsProvider, GameState} from '../context/index.js'
import React, { useContext, useEffect, Profiler, useRef, Suspense } from 'react'
import StatsForm from '../components/form/index'
import Header from '../components/header/index'
import MyTest from '../components/mytest/index'

const User = React.lazy(()=> import('../components/users/index'))


const Game = () => {
const { isName, setName } = useContext(GameState)
const refPo = useRef(null)
const refL = useRef(null)
const refD = useRef(null)
const refPu = useRef(null)
const refG = useRef(null)


useEffect(()=>{
  const getName = localStorage.getItem('name')
  if(getName.length > 0 ) { setName(true)}
},[isName])
function callBack(
 id, phase, actualDuration
)
{
console.log(id, phase,actualDuration)
}


  return (
  <div>
   {isName ? ( 
     <div>
      <Suspense fallback={
      <div>
       <h1>Ładowanie...</h1>
      </div>
    }>
       <Header {...{refPo, refL, refD, refPu, refG}}/>
       <MyTest />
       <div className="col-12 destination" ref={refPo}>Polana</div>
       <div className="col-12 destination" ref={refL}>Las</div>
       <div className="col-12 destination" ref={refD}>Dżungla</div>
       <div className="col-12 destination" ref={refPu}>Pustynia</div>
       <div className="col-12 destination" ref={refG}>Góry</div>
     <div className="game_wrapper">
       <Profiler id="UserStatsProvider" onRender={ callBack }>
     <UserStatsProvider>
     <User alive/>
     </UserStatsProvider>
     </Profiler>
     </div>
     </Suspense>
     </div>
   ) : (
     <StatsForm/>
   )}
  </div>
  );
}

export default Game;
