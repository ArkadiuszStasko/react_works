import styled from 'styled-components'

const CustomCircle = (props) => {

    const MyRectangle = styled.div`
    width: ${() => props.size.width + 'px'};
    height: ${() => props.size.height + 'px'};
    background-color: black;
    margin: 0 auto;
    display: flex;
    text-align: center;
    justify-content: center;
    transition: all 1s ease-in;
    border-radius: 100%;
    `

    return(
        <div>
            <h1>Wow wylosowałeś takie same wartości brawo !</h1>
            <MyRectangle/>
        </div>
    )
}

export default CustomCircle