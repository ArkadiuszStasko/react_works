import components from '../components'
import Text from '../text'
import withCustomDate from './index'

components.Text = withCustomDate(props => <Text {...props} />)
