import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navbar from "./components/navbar/index";
import Homepage from "./pages/homepage";
import World from "./pages/world";
import Character from './pages/character'
import SlowSuspense from "./components/slowSuspense/slowSuspense";
import Spinner from "./components/loader/index";
import { GameApp } from "./context/index.js";
import Rectangle from "./pages/reactangle/index";
import Circle from "./pages/circle/circle"
import TodoList from './pages/todoList/todoList'
import MainTaskPage from './pages/hocTask/index'
import Test from './components/restfulApi/index'

function App() {
  return (
    <div className="App">
      <Router>
        <GameApp>
          <Navbar />
          <Switch>
            
            <Route exact path="/" component={Homepage} />
            <Route path='/rectangle' component={Rectangle} />
            <Route path='/circle' component={Circle} />
            <Route path='/todoList' component={TodoList} />
            <Route path='/HocExample' component={MainTaskPage} />
            <Route path='/RestfulApi' component={Test} />

            <Route path="/user">
            <SlowSuspense fallback={<Spinner />}>
              <Character />
              </SlowSuspense>
            </Route>

            <Route path="/game">
            <SlowSuspense fallback={<Spinner />}>
                <World />
            </SlowSuspense>
            </Route>

          </Switch>
        </GameApp>
      </Router>
    </div>
  );
}

export default App;
