import {GameState} from '../context/index.js'
import { Link } from 'react-router-dom'
import React, { useContext, useEffect, } from 'react'
import StatsForm from '../components/form/index'

const User = React.lazy(()=> import('../components/users/index'))


const Homepage = () => {
const { isName, setName } = useContext(GameState)



useEffect(()=>{
  const getName = localStorage.getItem('name')
  if(!getName) { setName(true)}
},[isName])

  return (
  <div>
    {
      isName ? (
        <StatsForm/>
      ) : (
        <Link to='/game'>Świat</Link>
      )
    }
  </div>
  );
}

export default Homepage;
