import React, {useState} from 'react';
import components from './components'
import MainDatePage from './hocMonthTask/index'
require('./hoc')

const MainTaskPage = () => {
const [txt, setTxt] = useState('')

const {Test} = components

    return(
        <div>
            <h1>MainTaskPage</h1>
            <Test {...{txt}}/>
            <label>Text:</label>
            <input 
            name='name' 
            value={txt} 
            onChange={(e)=>setTxt(e.target.value)} 
            />
            <MainDatePage/>
        </div>
    )
}

export default MainTaskPage;