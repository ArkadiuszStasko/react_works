import React, { useState } from 'react'
import styled from 'styled-components'
import components from './components'

require('./hoc')

const Rectangle = () => {

    let [size, setSize] = useState({
        'width' : 103,
        'height' : 120,
        'max' : 5,
        'min' : 1,
     })

    const {RectangleResult} = components
    
const handleResize = () => {
        const max = size.max;
        const min = size.min;
        let width = Math.floor(Math.random() * (max - min) + min) * 50
        let height = Math.floor(Math.random() * (max - min) + min) * 50

        setSize(prevSize => ({
            ...prevSize,
            width,
            height
        })
           )

    }
let {width, height} = size
    return(
        <div>
       <RectangleResult {...{size}} />
       <p>Szerokość : {width} px</p>
       <p>Wysokość : {height} px</p>
       <button onClick={()=>handleResize()}>Generuj Nowy Element</button>
       </div>
    )
}

export default Rectangle;