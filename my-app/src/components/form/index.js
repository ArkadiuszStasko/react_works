import React, { useContext, useState, useEffect } from 'react'
//import User from '../users/index'
//import Oponent from '../oponent/index'
import {UserStatsProvider, OponentStatsProvider, GameState} from '../../context/index'
import './styles.css';

/*class StatsForm extends React.Component{
    constructor(props){
        super(props)
        this.state = {value: '', showStats: true}
        this.setState({ showStats: true})

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e){
this.setState({value: e.target.value})
    }

    handleSubmit(e){
        e.preventDefault();
        this.setState({ showStats: false}) 
        
    }

    

    render(){
        let showStats = this.state.showStats
        console.log(showStats)
        return(
            <div>
                    { showStats ?
        <form onSubmit={this.handleSubmit}>
            <label>Imię:</label>
            <input name='name' type='text' value={this.state.value} onChange={this.handleChange}/>
            <input name="send" type='submit'/>
        </form>
        :
        <div>
        <UserStatsProvider>
        <User name={this.state.value}/>
        </UserStatsProvider>
  
        <OponentStatsProvider>
        <Oponent/>
        </OponentStatsProvider>
        </div>
    }
        </div>
        )
    }
}*/

const StatsForm = () => {

    const {isName, setName} = useContext(GameState);
    const [name, addName] = useState('');

/*let myName;
    const createName = (item) => {
        return myName;
    }*/

const [myName, createName] = useState('');
useEffect(() =>{
    console.log(myName)
    createName('Arek')
    if(myName =='Arek' || myName =='Arek2'){
        createName('Arek2')
    }else{
        createName('Arek')
    }
},[myName])


    const handleSubmit = (e) => {
        //e.preventDefault();
        setName(true)
        localStorage.setItem('name', name)
    }
    return(
        <form onSubmit={e => handleSubmit(e)}>
        <label>Imię:</label>
        <input name='name' type='text' value={name} onChange={(e) =>addName(e.target.value)}/>
        <input name='send' type='submit' />
        </form>
    )
}


export default StatsForm