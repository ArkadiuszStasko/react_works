import './styles.css'

const List = ({ data }) => {

    const handleRemove = (id) => {
        data.setData(data.data.filter(data => data.id !== id))
    }

    return(
<div className='list-wrapper'>
    <li>{data.item.id}</li>
    <li>{data.item.title}</li>
    <li>{data.item.date}</li>
    <input type='checkbox'/>
    <form onSubmit={() => handleRemove(data.item.id)}>
    <button type='submit' >x</button>
    </form>
</div>
    )
}

export default List