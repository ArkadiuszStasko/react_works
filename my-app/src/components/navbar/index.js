import React from 'react'
import { Link } from 'react-router-dom'


const Navbar = () => {


    return(

        <div className="main_navbar-wrapper">
       <ul>
           <li>
               <Link to='/'>Home</Link>
           </li>
           <li>
               <Link to='/user'>Postać</Link>
           </li>
           <li>
               <Link to='/game'>Świat</Link>
           </li>
       </ul>
        </div>
    )
}

export default Navbar;