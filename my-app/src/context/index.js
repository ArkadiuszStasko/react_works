import React, {createContext, useState} from 'react'

export const GameState = createContext({
    isName: false,
    setName: item => {}
})


export const GameApp = ({ children }) => {
    const [isName, setName] = useState(false)

    return(
        <GameState.Provider
        value= {{
            isName,
            setName
        }}
        >
        { children }
        </GameState.Provider>
    )
}

export const CharStats = createContext({
    name: '',
    str: 0,
    hp: 0,
    speed: 0,
    dmg: 10,
    lvl: 1,
    exp: 0,
    maxexp: 200,
    addStr: item =>{},
    addName: item =>{},
    addExp: item =>{},
    addLvl: item =>{},
})

 export const UserStatsProvider = ({ children }) => {
    const [name, addName ]= useState('');
    const [str, addStr] = useState(10);
    const hp = 10;
    const speed = 10;
    const dmg = speed * str;
    const [lvl, addLvl]= useState(1);
    const [exp, addExp] = useState(0);
    const maxexp = 200;

    return(
        <CharStats.Provider
        value={{
            name,
            str,
            hp,
            speed,
            addStr,
            addName,
            dmg,
            lvl,
            addLvl,
            exp,
            maxexp,
            addExp,
        }}
        >
            {children}
        </CharStats.Provider>
    )
}

export const OponentStatsProvider = ({ children }) => {
    const name = 'Oponent';
    const str = Math.floor(Math.random() * (10 - 5)) + 5;
    const hp = Math.floor(Math.random() * (60 - 10)) + 10;
    const speed = 2;
    const dmg = speed * str;
    const lvl=1;
    return(
        <CharStats.Provider
        value={{
            name,
            str,
            hp,
            speed,
            dmg,
            lvl,
        }}
        >
            {children}
        </CharStats.Provider>
    )
}


