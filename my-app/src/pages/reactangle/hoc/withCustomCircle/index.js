import CustomCircle from './CustomCirle'

const withCustomCircle = Component => props => {

    console.log(props.size.width, props.size.height)
    return(
        <div>
            {
                props.size.width === props.size.height
                ? <CustomCircle {...props} />
                : <Component {...props} />
            }
        </div>
    )
}

export default withCustomCircle