import React from 'react'
import styled from 'styled-components'

const RectangleResult = ({...size}) => {

    const MyRectangle = styled.div`
    width: ${() => size.size.width + 'px'};
    height: ${() => size.size.height + 'px'};
    background-color: black;
    margin: 0 auto;
    display: flex;
    text-align: center;
    justify-content: center;
    transition: all 1s ease-in;
    `

    const Paragraph = styled.p`
    color: white;
    `

let volume = size.size.width * size.size.height;

    return(
<div>
<MyRectangle>
    <Paragraph>Objętość to : {volume}</Paragraph>
</MyRectangle>
</div>
    )
}

export default RectangleResult;