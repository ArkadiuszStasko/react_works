import components from '../../components'
import CustomTxt from './CustomText'

const withCustomText = Component => props => {
    return(
        <div>
            {
                props.txt === 'aaa'
                ? <CustomTxt {...props} />
                : <Component {...props} />
            }
        </div>
    )
}

export default withCustomText