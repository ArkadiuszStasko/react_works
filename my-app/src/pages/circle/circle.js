import React, {useCallback, useState} from 'react'
import './style.css'
import styled, {keyframes} from 'styled-components'

const CallbackFunction = ({showColor}) => {

    return(
        <button onClick={showColor}>Show Color</button>
    )
}


const Circle = () => {

let [nmb, setNmb] = useState(0)
const [color, setColor] = useState('')
const [length, setLength] = useState({
    widthLen: '',
    heightLen: '',
})

const colors = ['blue', 'pink', 'red', 'orange', 'grey', 'yellow', 'green', 'brown', 'gold', 'magenta']
var count = 0;

const run = (e) => {
        e.preventDefault();

        function change() {
            console.log(count)
            count++;
            if(count == colors.length) {
                count = 0;
            }
            setNmb(count)
        }
        
        setInterval(function(){
            change();
            setColor(colors[count])
        }, 3000);
        
        window.setInterval(animate, 2000 / 60)
    }

const showColor =  useCallback(
    () => {
        console.log(color)
        setColor(colors[count])
    },
    [color]
)

var x = 100 / 2; 
var y = 100 / 2;
var dx = 0;
var dy = 0;
var delta = 6; 

function animate() {

var dhx = (Math.random() * delta - delta / 2); 
var dhy = (Math.random() * delta - delta / 2);


dx += dhx;
dy += dhy;

if ((x + dx) < 0 || (x + dx) > 410) 
dx *= -0.5;
if ((y + dy) < 0 || (y + dy) > 120) dy *= -0.5;

x += dx;
y += dy;

setLength({...length,
    widthLen:  x,
    heightLen: y,
    })
}

    const Circle = styled.div`
    margin-top: ${length.heightLen + 'px'};
    margin-left: ${length.widthLen + 'px'};
    width: 30px;
    height: 30px;
    background-color: ${color ?
    (color) : ('black')};
    border-radius: 100%;
    `


    return (
        <div>
<div className='circle_container'>
    <Circle/>
</div>

<form onSubmit={e => run(e)}>
        <button>Start</button>
    </form>
    <form onSubmit={e => e.preventDefault()}>
    <CallbackFunction showColor={showColor}/>
    </form>
</div>
    )
}

export default Circle;