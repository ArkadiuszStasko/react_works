import React from 'react';
import Loader from 'react-loader-spinner';


const Spinner = () => {
    return (
        <div className='loader-wrapper'>
            <Loader
            type="Grid"
            color="W00BFFF"
            height={100}
            width={100}
            />
        </div>
    );


}

export default Spinner