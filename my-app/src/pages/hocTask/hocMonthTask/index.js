import React from 'react';
import components from './components'
require('./hoc/hocIndex')


export default class MainDatePage extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            date: ''
        }

        this.handleChange = this.handleChange.bind(this);
    }
    
    handleChange(event) {
        this.setState({date: event.target.value});
      }
    
    render(){

        const {Text} = components
        let date = this.state.date

        return(
            <div>
                <h1>MainDatePage</h1>
                <Text {...{date}}/>
                <h3>date wprowadź np w ten sposób: 2/9/2021</h3>
                <label>Wprowadź dzisiejszą date i zobacz co się stanie ;D :</label>
                <input 
                name='name' 
                value={this.state.date} 
                onChange={this.handleChange} 
                />
            </div>
        )
     }
    }

/*const MainDatePage = () => {
const [date, setDate] = useState('')

const {Text} = components

    return(
        <div>
            <h1>MainDatePage</h1>
            <Text {...{date}}/>
            <h3>date wprowadź np w ten sposób: 2/9/2021</h3>
            <label>Wprowadź dzisiejszą date i zobacz co się stanie ;D :</label>
            <input 
            name='name' 
            value={date} 
            onChange={(e)=>setDate(e.target.value)} 
            />
        </div>
    )
}

export default MainDatePage;*/