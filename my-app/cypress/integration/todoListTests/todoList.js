describe('todo list test',()=> {

    before(()=>{
        cy.log('rozpoczynam testowanie')
    })

    it('todo list page visit test', ()=>{
        cy.visit('http://localhost:3000/todolist')
    })

    it('Test dodawania zadania', ()=> {
        cy.contains('Add').click();
    })

    it('Test usuwania zadania',()=> {
        cy.contains('x').click();
    })

    after(()=>{
        cy.log('zakończyłem testowanie')
    })
})