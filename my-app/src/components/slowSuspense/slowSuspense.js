import React, {
    useEffect,
    useState,
    Suspense
} from 'react';


const SlowSuspense = ({ children, fallback = null }) => {
    const [show, setShow] = useState(true)
    useEffect(() => {
        setTimeout(() => setShow(false), 5000);
    }, [])

    return(
        <div>
            {show && fallback}
            <Suspense fallback={fallback}>
                { children }
            </Suspense>
        </div>
    )
}

export default SlowSuspense