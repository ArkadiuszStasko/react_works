import components from '../components'
import RectangleResult from '../rectangleResults'
import withCustomCircle from './withCustomCircle/index'


components.RectangleResult = withCustomCircle(props => <RectangleResult {...props}/>)