import React from 'react'
import MyMouse from './myMouse'
import MyTxt from './myText'

const MyTest = props => {

    return(
        <div>
           <MyMouse render={mousePos => (
              <MyTxt mousePos={mousePos}/>
           )
           }/>
        </div>
    )
}

export default MyTest